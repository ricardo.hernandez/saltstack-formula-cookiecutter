====
{{ cookiecutter.formula_name }}
====
{{ cookiecutter.formula_short_description }}

.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.

Features
========

Compatibility
=============

Available states
================

.. contents::
    :local:

``{{ cookiecutter.formula_name }}``
-----
Sub1
****

Running
=======

Ideas and future development
============================

Template
========

This formula was created from a cookiecutter template.

See https://github.com/richerve/saltstack-formula-cookiecutter.
